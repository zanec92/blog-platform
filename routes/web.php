<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', [
    'uses' => 'HomeController@index',
    'as' => 'dashboard'
]);

Route::get('/feed', [
    'uses' => 'HomeController@feed',
    'as' => 'dashboard.feed'
]);

Route::get('/blogs-list', [
    'uses' => 'BlogController@blogList',
    'as' => 'blog.list'
]);

Route::get('/blog/{blog_title}', [
    'uses' => 'BlogController@index',
    'as' => 'blog.index'
]);

Route::post('/createpost', [
    'uses' => 'PostController@postCreatePost',
    'as' => 'post.create',
    'middleware' => 'auth'
]);

Route::post('/edit', [
    'uses' => 'PostController@postEditPost',
    'as' => 'edit',
    'middleware' => 'auth'
]);

Route::get('/delete-post/{post_id}', [
    'uses' => 'PostController@getDeletePost',
    'as' => 'post.delete',
    'middleware' => 'auth'
]);

Route::post('/subscribe', [
    'uses' => 'SubscribeController@index',
    'as' => 'subscribe',
    'middleware' => 'auth'
]);
