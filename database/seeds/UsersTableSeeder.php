<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            'name' => 'Bloger1',
            'email' => 'test@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ],
        [
            'name' => 'Bloger2',
            'email' => 'test2@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ],
        [
            'name' => 'Bloger3',
            'email' => 'test3@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ],
        [
            'name' => 'Bloger4',
            'email' => 'test4@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ],
        [
            'name' => 'Bloger5',
            'email' => 'test5@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ],
        [
            'name' => 'Bloger6',
            'email' => 'test6@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ],
        [
            'name' => 'Bloger7',
            'email' => 'test7@test.ru',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10)
        ]
        ]);
    }
}
