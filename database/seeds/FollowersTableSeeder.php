<?php

use Illuminate\Database\Seeder;

class FollowersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('followers')->insert([
        [
            'user_id' => '1',
            'subscribe_id' => '2'
        ],
        [
            'user_id' => '1',
            'subscribe_id' => '4'
        ],
        [
            'user_id' => '1',
            'subscribe_id' => '6'
        ],
        [
            'user_id' => '2',
            'subscribe_id' => '1'
        ],
        [
            'user_id' => '2',
            'subscribe_id' => '3'
        ],
        [
            'user_id' => '2',
            'subscribe_id' => '5'
        ],
        [
            'user_id' => '3',
            'subscribe_id' => '6'
        ],
        [
            'user_id' => '4',
            'subscribe_id' => '1'
        ],
        [
            'user_id' => '4',
            'subscribe_id' => '2'
        ],
        [
            'user_id' => '4',
            'subscribe_id' => '3'
        ],
        [
            'user_id' => '5',
            'subscribe_id' => '2'
        ],
        [
            'user_id' => '5',
            'subscribe_id' => '7'
        ],
        [
            'user_id' => '6',
            'subscribe_id' => '2'
        ],
        [
            'user_id' => '6',
            'subscribe_id' => '1'
        ],
        [
            'user_id' => '7',
            'subscribe_id' => '2'
        ],
        [
            'user_id' => '7',
            'subscribe_id' => '5'
        ],
        [
            'user_id' => '7',
            'subscribe_id' => '1'
        ],
        [
            'user_id' => '7',
            'subscribe_id' => '3'
        ],
        [
            'user_id' => '7',
            'subscribe_id' => '4'
        ]
        ]);
    }
}
