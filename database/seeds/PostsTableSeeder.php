<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        DB::table('posts')->insert([
        [
            'body' => 'Привет!',
            'user_id' => '1',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Как дела?',
            'user_id' => '1',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Cras vel diam id felis pretium cursus. Aliquam sit amet.',
            'user_id' => '1',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Maecenas quis pretium quam. Donec ultricies lacus vitae neque sollicitudin.',
            'user_id' => '1',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Quisque consequat egestas nulla vitae rhoncus. Sed fermentum suscipit pharetra.',
            'user_id' => '1',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Donec euismod tellus vitae libero porta dapibus. Quisque rhoncus placerat.',
            'user_id' => '1',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Proin pellentesque sem libero. Vivamus bibendum, massa ac eleifend sollicitudin.',
            'user_id' => '2',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Mauris at dictum mi, nec hendrerit diam. Fusce pellentesque ultricies.',
            'user_id' => '2',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Nulla dapibus convallis neque. Nulla lacinia egestas elementum. Nulla sit.',
            'user_id' => '2',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Duis sit amet sem purus. Etiam nec luctus lacus. Quisque.',
            'user_id' => '3',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Nunc non dui risus. Duis vitae consequat libero. Mauris a.',
            'user_id' => '3',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Sed neque tellus, efficitur sed volutpat non, sagittis sit amet.',
            'user_id' => '4',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Pellentesque sed eros elit. Curabitur sit amet mollis neque, sit.',
            'user_id' => '5',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Aenean dapibus ornare hendrerit. Nulla vulputate augue sit amet magna.',
            'user_id' => '5',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Phasellus ac convallis sem. Cras venenatis, tortor aliquet varius finibus.',
            'user_id' => '5',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Sed molestie lectus in erat rutrum, ut lobortis lorem venenatis.',
            'user_id' => '7',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        [
            'body' => 'Quisque convallis bibendum finibus. Aenean feugiat, nisl eget sodales cursus.',
            'user_id' => '7',
            'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
        ],
        ]);
    }
}
