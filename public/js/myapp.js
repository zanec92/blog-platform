var postId = 0;
var postBodyElement = null;

$('.post').find('.interaction').find('.edit').on('click', function (event) {
    event.preventDefault();

    postBodyElement = event.target.parentNode.parentNode.childNodes[0];
    var postBody = postBodyElement.textContent;
    postId = event.target.parentNode.parentNode.dataset['postid'];
    $('#post-body').val(postBody);
    $('#edit-modal').modal();
});

$('#modal-save').on('click', function () {
    $.ajax({
        method: 'POST',
        url: urlEdit,
        data: {body: $('#post-body').val(), postId: postId, _token: token}
    })
    .done(function (msg) {
        $(postBodyElement).text(msg['new_body']);
        $('#edit-modal').modal('hide');
    });
});

$('#subscribe-button').on('click', function () {
    $.ajax({
        method: 'POST',
        url: urlSubscribe,
        data: {subscribe: $(this).data('subscribe'),
            blog: blogTitle,
            _token: token}
    })
    .done(function (msg) {
        $('#subscribe-button')
        .removeClass()
        .addClass(msg.class)
        .data('subscribe', msg.subscribe)
        .html(msg.text);
    });
});
