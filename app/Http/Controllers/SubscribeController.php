<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class SubscribeController extends Controller
{
    public function index(Request $request) {
        $response = [];
        $this->validate($request, [
            'subscribe' => 'required|numeric',
            'blog' => 'required'
        ]);
        $user = Auth::user();
        $blog = User::where('name', $request->blog)->first();
        if($request->subscribe === '1') {
            $user->followers()->attach($blog->id);
            $response['class'] = 'btn btn-danger subscribe-button';
            $response['subscribe'] = '0';
            $response['text'] = 'Отписаться';
            return response()->json($response, 200);
        }else {
            $user->followers()->detach($blog->id);
            $response['class'] = 'btn btn-primary subscribe-button';
            $response['subscribe'] = '1';
            $response['text'] = 'Подписаться';
            return response()->json($response, 200);
        }
    }
}
