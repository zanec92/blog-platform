<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('user_id', Auth::user()->id)->paginate(5);
        return view('home', compact('posts'));
    }
    
    public function feed() {
        $user = Auth::user();
        $posts = DB::table('followers')
                ->rightJoin('posts', 'followers.subscribe_id', '=', 'posts.user_id')
                ->rightJoin('users', 'posts.user_id', '=', 'users.id')
                ->where('followers.user_id', $user->id)
                ->where('posts.user_id', '!=', $user->id)
                ->select('posts.*', 'users.name as username')
                ->orderBy('created_at', 'desc')
                ->paginate(5);
        return view('blog.feed', compact('posts'));
    }
}
