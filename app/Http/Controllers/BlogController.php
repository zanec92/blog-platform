<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Auth;
use DB;

class BlogController extends Controller
{
    public function index($blogTitle) {
        if($blog = User::where('name', $blogTitle)->first()) {
            $posts = Post::where('user_id', $blog->id)->paginate(5);
        }else {
            abort(404);
        }
        $subscribe = DB::table('followers')
                ->select('*')
                ->where('user_id', Auth::user()->id)
                ->where('subscribe_id', $blog->id)
                ->get();
        return view('blog.index', compact('posts', 'blogTitle', 'subscribe'));
    }
    
    public function blogList() {
        $blogs = User::paginate(10);
        return view('blog.list', compact('blogs'));
    }
}
