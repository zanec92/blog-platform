<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    
    protected $fillable = [
        'post',
        'created_at',
        'user_id'
    ];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
