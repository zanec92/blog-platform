@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(Auth::user()->name != $blogTitle)
            
            <button class="btn btn-{{ ($subscribe->isEmpty()) ? 'primary' : 'danger' }} subscribe-button" type="button" 
                    id="subscribe-button" data-subscribe="{{ ($subscribe->isEmpty()) ? 1 : 0 }}">
                {{ ($subscribe->isEmpty()) ? 'Подписаться' : 'Отписаться' }}
            </button>
        @endif
        @include('includes.message')
        @include('includes.posts-list')
    </div>
</div>
<script>
    var blogTitle = '{{ $blogTitle }}';
    var urlSubscribe = '{{ route('subscribe') }}';
</script>
@endsection
