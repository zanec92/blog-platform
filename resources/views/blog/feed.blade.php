@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('includes.message')
        @include('includes.posts-list')
    </div>
</div>
@endsection
