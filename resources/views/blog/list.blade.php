@extends('layouts.app')

@section('content')
    @include('includes.message')
    <ul>
    @foreach($blogs as $blog)
        <li><a href="{{ action('BlogController@index', $blog->name) }}">{{ $blog->name }}</a></li>
    @endforeach
    </ul>
@endsection