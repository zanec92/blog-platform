@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('includes.message')
        <section class="row new-post">
            <div class="col-md-6 col-md-offset-3">
                <header><h3>Что нового?</h3></header>
                <form action="{{ route('post.create') }}" method="post">
                    <div class="form-group">
                        <textarea class="form-control" name="body" id="new-post" rows="5" placeholder="Ваш пост"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Сделать пост</button>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </form>
            </div>
        </section>
        @include('includes.posts-list')
    </div>
</div>
@endsection
