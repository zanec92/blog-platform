<section class="row posts">
    <div class="col-md-6 col-md-offset-3">
        <header><h3>Лента</h3></header>
        @forelse($posts as $post)
            <article class="post" data-postid="{{ $post->id }}">
                <p>{{ $post->body }}</p>
                <div class="info">
                    Запостил {{ $post->user->name or $post->username }} {{ $post->created_at }}
                </div>
                <div class="interaction">
                    @if(Auth::user()->id == $post->user_id)
                        <a href="#" class="edit">Редактировать</a> |
                        <a href="{{ route('post.delete', ['post_id' => $post->id]) }}">Удалить</a>
                    @endif
                </div>
            </article>
        @empty
            <p>Еще нет записей.</p>
        @endforelse
        {{ $posts->render() }}
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Редактировать запись</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="post-body">Редактировать запись</label>
                        <textarea class="form-control" name="post-body" id="post-body" rows="5"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="modal-save">Сохранить изменения</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <script>
    var token = '{{ Session::token() }}';
    var urlEdit = '{{ route('edit') }}';
</script>